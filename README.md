# README

Ce dépôt [git](https://git-scm.com/) contient l'ensemble des exemples et exercices du cours de POO.

Il est structuré en plusieurs modules
* `examples` : exemples utilisés dans les slides du cours
* `exercises` : exercices de TD
* `robot` : exercices du cours
* `misc` : divers autres exemples

## Construire le projet
Ce projet utilise [gradle](https://gradle.org/) pour la construction.
Il est nécessaire de disposer d'un [JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 8 (_Java Development Kit_) et d'une connexion internet lors de la construction du projet.

### Sous Linux
Il suffit de se placer dans le répertoire du projet et de taper

```bash
./gradlew build
```

### Sous Windows
Il suffit de se placer dans le répertoire du projet et de taper

```
gradlew build
```

## Ouvrir le projet
Les principaux IDE (_Integrated Development Environment_) pour Java peuvent importer les projets gradle.
C'est le cas d'[IntelliJ IDEA](https://www.jetbrains.com/idea/) et d'[Eclipse](https://www.eclipse.org/).

## Faire les exercices de TD
Pour faire un exercice, il est possible d'activer les tests associés.

### Commencer les exercices ou passer à l'exercice suivant

```bash
./gradlew nextExercise
```

### Lancer les tests

```bash
./gradlew test
```

### Activer tous les tests

```bash
./gradlew allExercises
```

### Réinitialiser les tests

```bash
./gradlew resetExercises
```
