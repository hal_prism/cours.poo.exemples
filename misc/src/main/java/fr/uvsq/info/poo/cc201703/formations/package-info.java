/**
 * Ce package propose une solution au CC de mars 2017.
 *
 * L'application doit permettre à des employés de s'inscrire à des formations.
 * Un employé peut s'inscrire à une formation si le nombre maximum de 15 inscrits
 * n'a pas été atteint.
 * L'implémentation doit comporter une classe Employe et une classe Formation.
 *
 */
package fr.uvsq.info.poo.cc201703.formations;