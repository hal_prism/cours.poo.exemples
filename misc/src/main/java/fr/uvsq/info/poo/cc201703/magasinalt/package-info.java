/**
 * Ce package propose une solution au CC de mars 2017 (alternative).
 *
 * Un magasin souhaite développer une application qui imprime un ticket de caisse.
 * Un ticket de caisse comprend une liste d’articles, et il est possible de calculer
 * le prix global de ces articles. Le prix d’un article est calculé en faisant
 * la somme de son prix hors taxe avec sa T.V.A.
 * En France la législation est que pour les articles alimentaires, la T.V.A est
 * dite “réduite” à un taux de 5,5% alors qu’elle est de 20% pour les autres articles.
 *
 * L’application devra permettre de :
 * – Créer des articles (alimentaires ou non alimentaires),
 * – Ajouter un article sur un ticket de caisse,
 * – Calculer le prix d’un article comme suit :
 * prix hors taxe + (prix hors taxe X taux de T.VA),
 * – Calculer le prix global (T.V.A comprise) d’un ensemble d’articles figurant
 * sur un ticket de caisse.
 *
 */
package fr.uvsq.info.poo.cc201703.magasinalt;