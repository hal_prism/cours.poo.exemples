package fr.uvsq.info.poo.cc201703.magasin;

import java.util.ArrayList;
import java.util.List;

public class Ticket {
    private List<Article> contenu;

    public Ticket() {
        contenu = new ArrayList<>();
    }

    public void ajouterArticle(Article article) {
        contenu.add(article);
    }

    @Override
    public String toString() {
        return contenu.toString() + " = " + getPrix();
    }

    public double getPrix() {
        double result = 0.0;
        for (Article article : contenu) {
            result += article.getPrix();
        }
        return result;
    }
}
