package fr.uvsq.info.poo.cc201703.magasin;

public class ArticleAlimentaire extends Article {
    private static final double TAUX_TVA = 5.5;

    private final String datePeremption;

    public ArticleAlimentaire(String code, int prix, String datePeremption) {
        super(code, prix);
        this.datePeremption = datePeremption;
    }

    @Override
    public String toString() {
        return String.format("ArticleAlimentaire{%s, %s, %.2f",
                super.toString(),
                datePeremption,
                getPrix());
    }

    @Override
    public double getTaux() {
        return TAUX_TVA;
    }
}
