package fr.uvsq.info.poo.cc201703.magasinalt;

public class Ticket {
    private static final int NB_ARTICLES_MAX = 2;

    private Article[] articles;
    private int nbArticles;

    public Ticket() {
        articles = new Article[NB_ARTICLES_MAX];
        nbArticles = 0;
    }

    @Override
    public String toString() {
        String ticketDesc = "Aucun article";
        if (nbArticles > 0) {
            ticketDesc = "";
            for (int i = 0; i < nbArticles; ++i) {
                ticketDesc += String.format("%s %.2f%n", articles[i].toString(), articles[i].prix());
            }
            ticketDesc += String.format("Total (TTC) = %.2f%n", prix());
        }
        return ticketDesc;
    }

    public boolean addArticle(Article item) {
        if (nbArticles < NB_ARTICLES_MAX) {
            articles[nbArticles++] = item;
            return true;
        }
        return false;
    }

    public double prix() {
        double total = 0.0;
        for (int i = 0; i < nbArticles; ++i) {
            total += articles[i].prix();
        }
        return total;
    }
}
