package fr.uvsq.info.poo.cc201703.magasinalt;

public class Article {
    private String intitule;
    private double prixHT;
    private Categorie categorie;

    public Article(String intitule, double prixHT, Categorie categorie) {
        this.intitule = intitule;
        this.prixHT = prixHT;
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %.2f", intitule, categorie, prixHT);
    }

    public double prix() {
        return prixHT * (1 + categorie.getTaux() / 100);
    }
}
