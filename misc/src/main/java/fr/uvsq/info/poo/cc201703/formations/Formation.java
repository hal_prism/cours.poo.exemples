package fr.uvsq.info.poo.cc201703.formations;

class Formation {
    private static final int EFFECTIF_MAX = 2;

    private String intitule;
    private String date;
    private Employe[] participants;
    private int nbParticipants;

    public Formation(final String intitule, final String date) {
        this.intitule = intitule;
        this.date = date;
        participants = new Employe[EFFECTIF_MAX];
        nbParticipants = 0;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        String participantsDesc = "";
        if (nbParticipants > 0) {
            participantsDesc += " [";
            for (int i = 0; i < nbParticipants; ++i) {
                participantsDesc += participants[i].toString();
                if (i < nbParticipants - 1) {
                    participantsDesc += ", ";
                }
            }
            participantsDesc += "]";
        }
        return String.format("%s (%s)%s", intitule, date, participantsDesc);
    }

    public boolean addEmploye(Employe employe) {
        if (nbParticipants < EFFECTIF_MAX) {
            participants[nbParticipants++] = employe;
            return true;
        }
        return false;
    }
}
