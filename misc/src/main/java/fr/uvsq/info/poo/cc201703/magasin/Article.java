package fr.uvsq.info.poo.cc201703.magasin;

public abstract class Article {
    private String codeBarre;
    private double prixHT;

    public Article(String codeBarre, double prixHT) {
        this.codeBarre = codeBarre;
        this.prixHT = prixHT;
    }

    public double getPrixHT() {
        return prixHT;
    }

    @Override
    public String toString() {
        return String.format("%s %.2f", codeBarre, prixHT);
    }

    public abstract double getTaux();

    public double getPrix() {
        return getPrixHT() * (1 + getTaux() / 100);
    }
}
