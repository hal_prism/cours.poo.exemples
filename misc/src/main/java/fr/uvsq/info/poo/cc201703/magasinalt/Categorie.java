package fr.uvsq.info.poo.cc201703.magasinalt;

public enum Categorie {
    ALIMENTAIRE(5.5), AUTRE(20.0);

    private double taux;

    Categorie(double taux) {
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }
}
