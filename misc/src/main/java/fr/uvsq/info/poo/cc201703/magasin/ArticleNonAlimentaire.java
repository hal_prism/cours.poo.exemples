package fr.uvsq.info.poo.cc201703.magasin;

/**
 * Created by hal on 20/03/17.
 */
public class ArticleNonAlimentaire extends Article {
    private static double TAUX_TVA = 20;
    public ArticleNonAlimentaire(String codeBarre, double prixHT) {
        super(codeBarre, prixHT);
    }

    @Override
    public String toString() {
        return "ArticleNonAlimentaire : " + super.toString() + ", " + getPrix();
    }

    @Override
    public double getTaux() {
        return TAUX_TVA;
    }
}
