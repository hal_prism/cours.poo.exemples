package fr.uvsq.info.poo.cc201703.formations;

class Employe {
    private String nom;
    private String prenom;
    private String phone;

    public Employe(String nom, String prenom, String phone) {
        this.nom = nom;
        this.prenom = prenom;
        this.phone = phone;
    }

    public Employe(String nom, String prenom) {
        this(nom, prenom, "");
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        String phoneDesc = phone.length() == 0 ? "" : String.format(" (%s)", phone);
        return String.format("%s %s%s", prenom, nom, phoneDesc);
    }

    public boolean addFormation(Formation formation) {
        return formation.addEmploye(this);
    }
}
