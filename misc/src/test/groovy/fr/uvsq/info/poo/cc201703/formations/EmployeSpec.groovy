package fr.uvsq.info.poo.cc201703.formations

import spock.lang.Specification

class EmployeSpec extends Specification {
    def "création d'employés"() {
        expect:
        employe.toString() == description

        where:
        employe                                      | description
        new Employe("Dupond", "Ariane")              | "Ariane Dupond"
        new Employe("Dupond", "Ariane", "061234")    | "Ariane Dupond (061234)"
        new Employe("Martin", "Cassiopée")           | "Cassiopée Martin"
        new Employe("Martin", "Cassiopée", "064321") | "Cassiopée Martin (064321)"
    }

    def "setPhone fixe le numéro de téléphone"() {
        given:
        def ariane = new Employe("Dupond", "Ariane")

        when:
        ariane.setPhone("061234")

        then:
        ariane.toString() == "Ariane Dupond (061234)"
    }

    def "addFormation inscrit l'employé à une formation"() {
        given:
        def ariane = new Employe("Dupond", "Ariane")
        def poo = new Formation("POO", "17/03/2017")

        when:
        boolean isInscrit = ariane.addFormation(poo)

        then:
        isInscrit
        poo.toString() == "POO (17/03/2017) [Ariane Dupond]"
    }

    def "addFormation échoue quand la formation est pleine"() {
        given:
        def ariane = new Employe("Dupond", "Ariane")
        def cassiopee = new Employe("Martin", "Cassiopée")
        def helios = new Employe("Martin", "Hélios")
        def poo = new Formation("POO", "17/03/2017")
        poo.addEmploye(ariane)
        poo.addEmploye(cassiopee)

        when:
        boolean isInscrit = helios.addFormation(poo)

        then:
        !isInscrit
        poo.toString() == "POO (17/03/2017) [Ariane Dupond, Cassiopée Martin]"
    }
}
