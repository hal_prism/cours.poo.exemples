package fr.uvsq.info.poo.cc201703.magasinalt

import spock.lang.Specification

class TicketSpec extends Specification{
    def ticket = new Ticket()
    def smartphone = new Article("Smartphone", 100, Categorie.AUTRE)
    def pain = new Article("Pain", 1, Categorie.ALIMENTAIRE)

    def "création d'un ticket vide"() {
        expect:
        ticket.toString() == "Aucun article"
    }

    def "ajout d'un article"() {
        when:
        def isAdded = ticket.addArticle(smartphone)

        then:
        isAdded
        ticket.prix() == 120.0D
        ticket.toString() == String.format("Smartphone (AUTRE) 100,00 120,00%nTotal (TTC) = 120,00%n")
    }

    def "ajout d'un second article"() {
        given:
        ticket.addArticle(smartphone)

        when:
        def isAdded = ticket.addArticle(pain)

        then:
        isAdded
        ticket.prix() == 121.055D
        ticket.toString() == String.format("Smartphone (AUTRE) 100,00 120,00%nPain (ALIMENTAIRE) 1,00 1,06%nTotal (TTC) = 121,06%n")
    }

    def "Échec de l'ajout d'un 3ème article"() {
        given:
        ticket.addArticle(smartphone)
        ticket.addArticle(pain)

        when:
        def isAdded = ticket.addArticle(pain)

        then:
        !isAdded
        ticket.prix() == 121.055D
        ticket.toString() == String.format("Smartphone (AUTRE) 100,00 120,00%nPain (ALIMENTAIRE) 1,00 1,06%nTotal (TTC) = 121,06%n")
    }
}
