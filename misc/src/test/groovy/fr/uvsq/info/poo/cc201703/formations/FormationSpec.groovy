package fr.uvsq.info.poo.cc201703.formations

import spock.lang.Specification

class FormationSpec extends Specification {
    def "création de formations"() {
        expect:
        formation.toString() == description
        formation.getDate() == date

        where:
        formation | description | date
        new Formation("POO", "17/03/2017") | "POO (17/03/2017)" | "17/03/2017"
        new Formation("Langage C", "10/02/2017") | "Langage C (10/02/2017)" | "10/02/2017"
    }

    def "inscription d'un employé à une formation"() {
        given:
        def poo = new Formation("POO", "17/03/2017")
        def ariane = new Employe("Dupond", "Ariane")
        def cassiopee = new Employe("Martin", "Cassiopée")

        when:
        poo.addEmploye(ariane)
        poo.addEmploye(cassiopee)

        then:
        poo.toString() == "POO (17/03/2017) [Ariane Dupond, Cassiopée Martin]"
    }

    def "l'inscription échoue quand la formation est complète"() {
        given:
        def poo = new Formation("POO", "17/03/2017")
        def ariane = new Employe("Dupond", "Ariane")
        def cassiopee = new Employe("Martin", "Cassiopée")
        def helios = new Employe("Martin", "Hélios")
        poo.addEmploye(ariane)
        poo.addEmploye(cassiopee)

        when:
        def isInscrit = poo.addEmploye(helios)

        then:
        !isInscrit
        poo.toString() == "POO (17/03/2017) [Ariane Dupond, Cassiopée Martin]"
    }
}
