package fr.uvsq.info.poo.cc201703.magasinalt

import spock.lang.Specification

class ArticleSpec extends Specification {
    static final smartphone = new Article("Smartphone", 100, Categorie.AUTRE)
    static final pain = new Article("Pain", 1, Categorie.ALIMENTAIRE)

    def "création d'articles"() {
        expect:
        article.toString() == description

        where:
        article                         | description
        smartphone | "Smartphone (AUTRE) 100,00"
        pain | "Pain (ALIMENTAIRE) 1,00"
    }

    def "prix retourne le prix TTC des articles"() {
        expect:
        article.prix() == prixTTC

        where:
        article                         | prixTTC
        smartphone | 100.00 * 1.2
        pain | 1.00 * 1.055
    }
}
