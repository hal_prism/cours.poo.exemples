package step1;

import fr.uvsq.info.poo.robot.step1.Robot;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TestRobot {
	@Mock
	private Terrain terrain;
	private Robot robot;

	@Before
	public void setUp() throws Exception {
		robot = new Robot(terrain, new Position(0,0), Direction.OUEST);
	}

	@Test
	public void testDeplacementReussi() {
		robot.avance();
		Mockito.verify(terrain).avancerRobot(robot);
	}
	
	@Test
	public void testDeplacementEchoue() {
		Mockito.when(terrain.avancerRobot(robot)).thenReturn(false);
		boolean isOK = robot.avance();
		Mockito.verify(terrain).avancerRobot(robot);
		assertEquals(false, isOK);
	}
}
