package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente un type de robot particulier.
 * Ces robots ont la capacité de transporter un objet.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
class Transporteur extends Robot {
    /** Surpoids des robots transporteurs */
    public static final int SURPOIDS = 1;

    /** L'objet transporté. */
    private ObjetTransportable charge;

    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer.
     * @param depart position de départ du robot.
     * @param direction orientation du robot.
     */
    public Transporteur(Terrain terrain, Position depart, Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Retourne le poids du transporteur
     * éventuellement chargé.
     * @return le poids de l'élément.
     */
    @Override
    public int getPoids() {
        return super.getPoids() +
            SURPOIDS +
            (charge != null ? charge.getPoids() : 0);
    }

    /**
     * Prends un objet sur le terrain.
     */
    public void ramasse() {
	this.charge = terrain.ramasserDevant(this);
    }

    /**
     * Dépose l'objet transporté sur le terrain.
     */
    public void depose() {
	terrain.deposerDevant(this, charge);
	charge = null;
    }

    /*
     * Décrit la programmation du robot.
     */
    public void action() {
	super.action();
	ramasse();
	avance();
	depose();
    }
}
