package fr.uvsq.info.poo.robot.step3;

import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

/**
 * Représente l'application.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
enum RobotApplication {
    ENVIRONNEMENT;

    /*
     * Méthode principale du programme.
     */
    public void run(String[] args) {
        // tag::robot-app-3[]
        // Création du terrain
        Terrain terrain = new Terrain(4, 5, null);

        // Création des robots
        final int NB_ROBOTS = 3;
        Actionable[] robots = new RobotWithAction[NB_ROBOTS];
        robots[0] = new RobotWithAction(terrain, new Position(0, 0),
                Direction.EST);
        robots[1] = new TransporteurWithAction(terrain, new Position(1, 1),
                Direction.EST);
        robots[2] = new DestructeurWithAction(terrain, new Position(3, 0),
                Direction.SUD);

        // Exécution des actions des robots
        for (int i = 0; i < NB_ROBOTS; ++i) {
            robots[i].action();
        }
        // end::robot-app-3[]

        System.out.println(terrain);
    }

    public static void main(String[] args) {
        ENVIRONNEMENT.run(args);
    }
}
