package fr.uvsq.info.poo.robot.step3;

/**
 * Permet aux robots d'implémenter un action.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public interface Actionable {
    /**
     * L'action à exécuter.
     */
    void action();
}
