package fr.uvsq.info.poo.robot.step2;

import fr.uvsq.info.poo.robot.step1.Robot;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

// tag::transporteur-2[]
/**
 * Cette classe représente un type de robot particulier.
 * Ces robots ont la capacité de transporter un objet.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public class Transporteur extends Robot {
    /** L'objet transporté. */
    private Object charge;

    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer.
     * @param depart position de départ du robot.
     * @param orientation orientation du robot.
     */
    public Transporteur(Terrain terrain, Position depart,
                        Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Prends un objet sur le terrain.
     * @param charge objet à transporter.
     */
    public void ramasse(Object charge) {
        this.charge = charge;
    }

    /**
     * Dépose l'objet transporté.
     * @return l'objet transporté.
     */
    public Object depose() {
        Object tmp = charge;
        charge = null;
        return tmp;
    }
}
// end::transporteur-2[]
