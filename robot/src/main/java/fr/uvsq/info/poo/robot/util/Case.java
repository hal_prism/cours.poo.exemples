package fr.uvsq.info.poo.robot.util;

import fr.uvsq.info.poo.robot.step1.Robot;

/**
 * Cette classe représente une case du terrain.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public class Case {
    /** Décors possibles sur la case. */
    public enum Decor { VIDE, MUR; }

    /** Le décor. */
    private Decor decor;

    /** Le robot si présent, null sinon. */
    private Robot robot;

    /**
     * Initialise une case sans décor.
     */
    public Case() {
	decor = Decor.VIDE;
    }

    /**
     * Récupère le décor.
     *
     * @return le décor.
     */
    public Decor getDecor() {
	return decor;
    }

    /**
     * Change le décor.
     *
     * @param d le nouveau décor.
     */
    public void setDecor(Decor d) {
	decor = d;
    }

    /**
     * Récupère le robot.
     *
     * @return le robot ou null.
     */
    public Robot getRobot() {
	return robot;
    }

    /**
     * Installe un robot sur la case.
     *
     * @param r le robot ou null.
     */
    public void setRobot(Robot r) {
	robot = r;
    }
}
