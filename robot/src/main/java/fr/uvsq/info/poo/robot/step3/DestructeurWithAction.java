package fr.uvsq.info.poo.robot.step3;

import fr.uvsq.info.poo.robot.step2.Destructeur;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

/**
 * Cette classe représente un type de robot particulier.
 * Ces robots ont la capacité de détruire un obstacle
 * se trouvant devant eux.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public class DestructeurWithAction extends Destructeur implements Actionable {
    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer.
     * @param depart position de départ du robot.
     * @param orientation orientation du robot.
     */
    public DestructeurWithAction(Terrain terrain, Position depart,
                       Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Décrit la programmation du robot.
     */
    @Override
    public void action() {
        for (int i = 0; i < 4; ++i) {
            detruit(); avance();
        }
    }
}
