package fr.uvsq.info.poo.robot.step2;

import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

/**
 * Représente l'application.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
enum RobotApplication {
    ENVIRONNEMENT;

    /*
     * Méthode principale du programme.
     */
    public void run(String[] args) {
        // tag::robot-app-2[]
        // Création du terrain
        Position[] murs = {new Position(1, 0), new Position(1, 1),
                new Position(1, 2)};
        Terrain terrain = new Terrain(3, 3, murs);

        // Création des robots
        Transporteur transp = new Transporteur(terrain, new Position(0, 0),
                Direction.SUD);
        Destructeur destr = new Destructeur(terrain, new Position(0, 1),
                Direction.EST);

        // Actions des robots
        destr.detruit();
        destr.tourneADroite();
        destr.avance();
        transp.ramasse(new Object());
        transp.avance();
        transp.tourneADroite();
        transp.tourneADroite();
        transp.tourneADroite();
        transp.avance();
        transp.avance();
        transp.tourneADroite();
        transp.avance();
        transp.depose();
        // end::robot-app-2[]
        System.out.println(terrain);
    }

    public static void main(String[] args) {
        ENVIRONNEMENT.run(args);
    }
}
