package fr.uvsq.info.poo.robot.step3;

import fr.uvsq.info.poo.robot.step2.Transporteur;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

class TransporteurWithAction extends Transporteur implements Actionable {
    public TransporteurWithAction(Terrain terrain, Position depart,
                                  Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Réalise l'action du transporteur.
     */
    @Override
    public void action() {
        ramasse(new Object());
        avance();
        depose();
    }
}
