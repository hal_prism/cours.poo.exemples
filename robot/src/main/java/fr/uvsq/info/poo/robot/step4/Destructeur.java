package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente un type de robot particulier.
 * Ces robots ont la capacité de détruire un obstacle
 * se trouvant devant eux.
 *
 * @author Stéphane Lopes
 * @version oct. 2008
 */
class Destructeur extends Robot {
    /**
     * Surpoids des robots destructeurs
     */
    public static final int SURPOIDS = 2;

    /**
     * Initialise le robot.
     *
     * @param terrain   terrain sur lequel le robot va évoluer.
     * @param depart    position de départ du robot.
     * @param direction orientation du robot.
     */
    public Destructeur(Terrain terrain, Position depart, Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Retourne le poids du robot.
     *
     * @return le poids de l'élément.
     */
    @Override
    public int getPoids() {
        return super.getPoids() + SURPOIDS;
    }

    /**
     * Supprime le contenu de la case se trouvant devant le robot
     */
    public void detruit() {
        terrain.detruitCaseDevant(this);
    }

    /*
     * Décrit la programmation du robot.
     */
    public void action() {
        for (int i = 0; i < 4; ++i) {
            detruit();
            avance();
        }
    }
}
