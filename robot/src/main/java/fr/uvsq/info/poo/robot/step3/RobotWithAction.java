package fr.uvsq.info.poo.robot.step3;

import fr.uvsq.info.poo.robot.step1.Robot;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

/**
 * Cette classe représente un robot pouvant évoluer sur un terrain.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public class RobotWithAction extends Robot implements Actionable {
    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer
     * @param orientation orientation du robot
     */
    public RobotWithAction(Terrain terrain, Position depart, Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Réalise l'action du robot.
     */
    @Override
    public void action() {
        for (int i = 0; i < 4; ++i) {
            avance(); avance();
            tourneADroite();
        }
    }
}
