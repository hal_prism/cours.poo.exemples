package fr.uvsq.info.poo.robot.step1;

import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

// tag::robot-step1[]
/**
 * Cette classe représente un robot pouvant évoluer sur un terrain.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public class Robot {
    /** Terrain sur lequel évolue le robot. */
    protected Terrain terrain;

    /** Orientation du robot. */
    private Direction orientation;

    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va evoluer
     * @param depart position d'origine du robot
     * @param orientation orientation du robot
     */
    public Robot(Terrain terrain, Position depart, Direction orientation) {
        assert terrain != null;
        this.terrain = terrain;
        terrain.ajouterRobot(this, depart);
        this.orientation = orientation;
    }

    /**
     * Retourne l'orientation du robot.
     * @return orientation du robot
     */
    public Direction getOrientation() {
        return orientation;
    }

    /**
     * Demande au robot d'avancer d'une case.
     * @return true si l'operation a ete effectuee
     */
    public boolean avance() {
        return terrain.avancerRobot(this);
    }

    /**
     * Fait faire au robot une rotation d'un quart de tour a droite.
     */
    public void tourneADroite() {
        orientation = orientation.next();
    }
}
// end::robot-step1[]
