package fr.uvsq.info.poo.robot.util;

/**
 * Cette classe représente une position sur le terrain.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
public class Position {
    /**
     * L'abscisse de la position.
     */
    private int x;
    /**
     * L'ordonnée de la position.
     */
    private int y;

    /**
     * Construit une position à partir de deux coordonnées.
     *
     * @param x abscisse de la position
     * @param y ordonnée de la position
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Récupère l'abscisse de la position.
     *
     * @return l'abscisse de la position.
     */
    public int getAbscisse() {
        return x;
    }

    /**
     * Récupère l'ordonnée de la position.
     *
     * @return l'ordonnée de la position.
     */
    public int getOrdonnee() {
        return y;
    }
}
