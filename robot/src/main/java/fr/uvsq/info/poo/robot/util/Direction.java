package fr.uvsq.info.poo.robot.util;

/**
 * Définit les directions possibles.
 *
 * @version jan. 2017
 * @author Stéphane Lopes
 */
public enum Direction {
    NORD, EST, SUD, OUEST;

    /**
     * Retourne la direction suivante (rotation horaire de 90°).
     * @return la direction suivante.
     */
    public Direction next() {
        // values() retourne un tableau des constantes dans l'ordre de déclaration
        // ordinal() retourne l'indice de la constante
        return values()[(ordinal() + 1) % values().length];
    }
}

// ATTENTION
//L'approche suivante génère une classe pour chaque constante.
//enum Direction {
//    NORD { public Direction next() { return EST;} },
//    EST { public Direction next() { return SUD;} },
//    SUD { public Direction next() { return OUEST;} },
//    OUEST { public Direction next() { return NORD;} };
//
//    /**
//     * Retourne la direction suivante (rotation horaire de 90�).
//     * @return la direction suivante.
//     */
//    public abstract Direction next();
//}
