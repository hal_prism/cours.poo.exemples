package fr.uvsq.info.poo.robot.step4;

/**
 * Représente l'application.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
enum RobotApplication {
    ENVIRONNEMENT;

    /*
     * Méthode principale du programme.
     */
    public void run(String[] args) {
        // Création du terrain
        Terrain terrain = new Terrain(4, 5, null);

        // Création des éléments mobiles
        final int NB_ELEMENTS = 5;
        ElementMobile[] elements = new ElementMobile[NB_ELEMENTS];
        elements[0] = new Robot(terrain, new Position(0, 0), Direction.EST);
        elements[1] = new Transporteur(terrain, new Position(1, 1),
                Direction.EST);
        elements[2] = new Destructeur(terrain, new Position(3, 0),
                Direction.SUD);
        elements[3] = new ObjetTransportable(terrain,
                new Position(3, 4), 5);
        elements[4] = new ObjetTransportable(terrain,
                new Position(1, 2), 2);

        // Calcul du poids total
        int poidsTotal = 0;
        for (int i = 0; i < NB_ELEMENTS; ++i) {
            poidsTotal += elements[i].getPoids();
        }
        assert poidsTotal == (1 + 3 + 2 + 5 + 2) : poidsTotal;
    }

    public static void main(String[] args) {
        ENVIRONNEMENT.run(args);
    }
}
