package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente une case du terrain.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
public class Case {
    /** Décors possibles sur la case. */
    public enum Decor { VIDE, MUR; }

    /** Le décor. */
    private Decor decor;

    /** Le robot si présent, null sinon. */
    private Robot robot;

    /** Un objet si présent, null sinon. */
    private ObjetTransportable objet;

    /**
     * Initialise une case sans décor.
     */
    public Case() {
	decor = Decor.VIDE;
    }

    /**
     * Récupère le décor.
     *
     * @return le décor.
     */
    public Decor getDecor() {
	return decor;
    }

    /**
     * Change le décor.
     *
     * @param d le nouveau décor.
     */
    public void setDecor(Decor d) {
	decor = d;
    }

    /**
     * Place un élément sur la case.
     *
     * @param elt l'élément à placer.
     */
    public void set(ElementMobile elt) {
	if (elt instanceof ObjetTransportable) {
	    setObjet((ObjetTransportable)elt);
	} else {
	    setRobot((Robot)elt);
	}
    }

    /**
     * Récupère le robot.
     *
     * @return le robot ou null.
     */
    public Robot getRobot() {
	return robot;
    }

    /**
     * Installe un robot sur la case.
     *
     * @param r le robot ou null.
     */
    public void setRobot(Robot r) {
	robot = r;
    }

    /**
     * Récupère l'objet.
     *
     * @return l'objet ou null.
     */
    public ObjetTransportable getObjet() {
	return objet;
    }

    /**
     * Installe un objet sur la case.
     *
     * @param o l'objet ou null.
     */
    public void setObjet(ObjetTransportable o) {
	objet = o;
    }
}
