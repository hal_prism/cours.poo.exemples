package fr.uvsq.info.poo.robot.step2;

import fr.uvsq.info.poo.robot.step1.Robot;
import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

// tag::destructeur-2[]
/**
 * Cette classe représente un type de robot particulier.
 * Ces robots ont la capacité de détruire un obstacle
 * se trouvant devant eux.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
public class Destructeur extends Robot {
    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer.
     * @param depart position de départ du robot.
     * @param orientation orientation du robot.
     */
    public Destructeur(Terrain terrain, Position depart,
                       Direction orientation) {
        super(terrain, depart, orientation);
    }

    /**
     * Supprime le contenu de la case se trouvant devant le robot
     */
    public void detruit() {
        terrain.detruitCaseDevant(this);
    }
}
// end::destructeur-2[]
