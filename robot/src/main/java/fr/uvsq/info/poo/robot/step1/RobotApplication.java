package fr.uvsq.info.poo.robot.step1;

import fr.uvsq.info.poo.robot.util.Direction;
import fr.uvsq.info.poo.robot.util.Position;
import fr.uvsq.info.poo.robot.util.Terrain;

/**
 * Représente l'application.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
enum RobotApplication {
    ENVIRONNEMENT;

    /*
    * Méthode principale du programme.
    */
    public void run(String[] args) {
        // tag::robot-app[]
        // Creation du terrain
        Position[] murs = { new Position(0, 1), new Position(2, 0) };
        Terrain terrain = new Terrain(3, 2, murs);

        // Creation des robots
        Robot r2d2 = new Robot(terrain, new Position(0, 0),
                Direction.NORD);
        Robot c3po = new Robot(terrain, new Position(2, 1),
                Direction.OUEST);

        // Mouvements
        r2d2.tourneADroite();    // (0, 0) vers l'est
        r2d2.avance();        // (1, 0) vers l'est
        r2d2.tourneADroite();    // (1, 0) vers le sud
        r2d2.avance();        // (1, 1) vers le sud
        // end::robot-app[]

        System.out.println(terrain);
    }

    public static void main(String[] args) {
        ENVIRONNEMENT.run(args);
    }
}
