package fr.uvsq.info.poo.robot.util;

import fr.uvsq.info.poo.robot.step1.Robot;
import fr.uvsq.info.poo.robot.step2.Destructeur;

/**
 * Cette classe représente le terrain sur lequel vont évoluer les robots.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
public class Terrain {
    /**
     * Le terrain ou vont évoluer les robots.
     */
    private Case[][] terrain;

    /**
     * Initialise le terrain.
     *
     * @param tailleX taille du terrain (en X).
     * @param tailleY taille du terrain (en Y).
     * @param murs    la liste des murs à placer sur le terrain.
     */
    public Terrain(int tailleX, int tailleY, Position[] murs) {
        assert tailleX > 0 && tailleY > 0;
        terrain = new Case[tailleX][tailleY];
        for (int i = 0; i < tailleX; ++i) {
            for (int j = 0; j < tailleY; ++j) {
                terrain[i][j] = new Case();
            }
        }
        if (murs != null) {
            for (int i = 0; i < murs.length; ++i) {
                assert estValide(murs[i]);
                terrain[murs[i].getAbscisse()][murs[i].getOrdonnee()].setDecor(Case.Decor.MUR);
            }
        }
    }

    /**
     * Place un robot sur le terrain.
     *
     * @param robot robot à ajouter
     * @param orig  position du robot
     */
    public void ajouterRobot(Robot robot, Position orig) {
        assert robot != null && estValide(orig);
        terrain[orig.getAbscisse()][orig.getOrdonnee()].setRobot(robot);
    }

    /**
     * Déplace le robot d'une case vers l'avant.
     *
     * @param robot robot à déplacer
     */
    public boolean avancerRobot(Robot robot) {
        Position dest = caseDevantRobot(robot);
        int x = dest.getAbscisse(), y = dest.getOrdonnee();
        if (estValide(dest) &&
                terrain[x][y].getDecor() == Case.Decor.VIDE &&
                terrain[x][y].getRobot() == null) {
            Position orig = chercherRobot(robot);
            terrain[orig.getAbscisse()][orig.getOrdonnee()].setRobot(null); // Pas très élégant...
            terrain[x][y].setRobot(robot);
            return true;
        }
        return false;
    }

    /**
     * Supprime le décor de la case devant le robot destructeur.
     *
     * @param destr le robot destructeur.
     */
    public void detruitCaseDevant(Destructeur destr) {
        Position dest = caseDevantRobot(destr);
        if (estValide(dest)) {
            terrain[dest.getAbscisse()][dest.getOrdonnee()].setDecor(Case.Decor.VIDE);
        }
    }

    /**
     * Dessine le jeu
     *
     * @return une chaine representant le jeu
     */
    public String toString() {
        StringBuilder jeu = new StringBuilder();
        for (int j = 0; j < terrain[0].length; ++j) {
            for (int i = 0; i < terrain.length; ++i) {
                if (terrain[i][j].getDecor() == Case.Decor.MUR) {
                    jeu.append("|  M  |");
                } else if (terrain[i][j].getRobot() != null) {
                    jeu.append("| R ");
                    switch (terrain[i][j].getRobot().getOrientation()) {
                        case NORD:
                            jeu.append("N");
                            break;
                        case EST:
                            jeu.append("E");
                            break;
                        case SUD:
                            jeu.append("S");
                            break;
                        case OUEST:
                            jeu.append("O");
                            break;
                        default:
                            assert false;
                    }
                    jeu.append(" |");
                } else {
                    jeu.append("|     |");
                }
            }
            jeu.append("\n");
        }
        return jeu.toString();
    }

    /**
     * Retourne la position du robot.
     *
     * @param robot le robot cherché.
     * @return null si le robot n'est pas trouvé, sa position sinon.
     */
    private Position chercherRobot(Robot robot) {
        for (int i = 0; i < terrain.length; ++i) {
            for (int j = 0; j < terrain[i].length; ++j) {
                if (terrain[i][j].getRobot() == robot) {
                    return new Position(i, j);
                }
            }
        }
        return null;
    }

    /**
     * Retourne la position de la case devant le robot.
     *
     * @param robot le robot cherché.
     * @return null si le robot n'est pas trouvé, la position cherchée sinon.
     */
    private Position caseDevantRobot(Robot robot) {
        Position orig = chercherRobot(robot);
        if (orig == null) return null;
        int x = orig.getAbscisse(), y = orig.getOrdonnee();
        switch (robot.getOrientation()) {
            case NORD:
                --y;
                break;
            case EST:
                ++x;
                break;
            case SUD:
                ++y;
                break;
            case OUEST:
                --x;
                break;
            default:
                assert false;
        }
        return new Position(x, y);
    }

    /**
     * Teste si une position est dans le terrain
     *
     * @return true si la position est valide
     */
    private boolean estValide(Position p) {
        int x = p.getAbscisse(), y = p.getOrdonnee();
        return x >= 0 && x < terrain.length &&
                y >= 0 && y < terrain[x].length;
    }
}
