package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente un objet pouvant
 * être transporté par un robot adapté.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
public class ObjetTransportable extends ElementMobile {
    /** Le poids par défaut des objets */
    public static final int POIDS_PAR_DEFAUT = 1;

    /** Le poids de l'objet */
    private int poids;

    /**
     * Initialise l'objet.
     * @param terrain terrain sur lequel l'objet se trouve
     * @param depart position de départ sur le terrain
     * @param poids le poids de l'objet.
     */
    public ObjetTransportable(Terrain terrain, Position depart, int poids) {
        super(terrain, depart);
        this.poids = poids;
    }

    /**
     * Initialise un objet de poids 1.
     * @param terrain terrain sur lequel l'objet se trouve
     * @param depart position de départ sur le terrain
     */
    public ObjetTransportable(Terrain terrain, Position depart) {
        this(terrain, depart, POIDS_PAR_DEFAUT);
    }

    /**
     * Retourne le poids de l'élément.
     * @return le poids de l'élément.
     */
    @Override
    public int getPoids() {
        return poids;
    }
}
