package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente un élement mobile
 * pouvant se trouver sur une case du terrain.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
public abstract class ElementMobile {
    /** Terrain sur lequel se trouve l'élément. */
    protected Terrain terrain;

    /**
     * Place l'élément sur un terrain à une position donnée.
     * @param terrain terrain sur lequel l'élément va évoluer
     * @param depart position de départ sur le terrain
     */
    protected ElementMobile(Terrain terrain, Position depart) {
        assert terrain != null && depart != null;
        this.terrain = terrain;
        terrain.ajouter(this, depart);
    }

    /**
     * Retourne le poids de l'élément.
     * @return le poids de l'élément.
     */
    public abstract int getPoids();
}
