package fr.uvsq.info.poo.robot.step4;

/**
 * Cette classe représente un robot pouvant évoluer sur un terrain.
 *
 * @version oct. 2008
 * @author Stéphane Lopes
 */
public class Robot extends ElementMobile {
    /** Le poids par défaut des robots */
    public static final int POIDS_PAR_DEFAUT = 1;

    /** Orientation du robot. */
    private Direction orientation;

    /**
     * Initialise le robot.
     * @param terrain terrain sur lequel le robot va évoluer
     * @param depart position de départ sur le terrain
     * @param orientation orientation du robot
     */
    public Robot(Terrain terrain, Position depart, Direction orientation) {
        super(terrain, depart);
        this.orientation = orientation;
    }

    /**
     * Retourne le poids du robot.
     * @return le poids de l'élément.
     */
    @Override
    public int getPoids() {
        return POIDS_PAR_DEFAUT;
    }

    /**
     * Retourne l'orientation du robot.
     * @return orientation du robot
     */
    public Direction getOrientation() {
	return orientation;
    }

    /**
     * Demande au robot d'avancer d'une case.
     * @return true si l'opération a été effectuée
     */
    public boolean avance() {
	return terrain.avancerRobot(this);
    }

    /**
     * Fait faire au robot une rotation d'un quart de tour à droite.
     */
    public void tourneADroite() {
	orientation = orientation.next();
    }

    /**
     * Décrit la programmation du robot.
     */
    public void action() {
	for (int i = 0; i < 4; ++i) {
	    avance(); avance();
	    tourneADroite();
	}
    }
}
