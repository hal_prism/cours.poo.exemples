package fr.uvsq.info.poo.io;

import java.io.*;
import java.time.LocalDate;
import java.util.Arrays;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

public class SerializationDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Student[] students = {
                new Student(1, "Ariane"),
                new Student(2, "Cassiopée"),
                new Student(3, "Hélios")
        };

        DummyFile objectStore = new DummyFile();

        writeObjectsToFile(students, objectStore.toString());

        Student[] readStudents = null;
        LocalDate date = readObjectsFromFile(readStudents, objectStore.toString());

        System.out.format("Liste des étudiants au %s :%n", date.format(ISO_LOCAL_DATE));
        System.out.println(Arrays.toString(students));
    }

    // tag::writeObjectsToFile[]
    private static void writeObjectsToFile(Student[] students, String filename) throws IOException {
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(
                             new FileOutputStream(filename))
        ) {
            oos.writeObject(LocalDate.now());
            oos.writeObject(students);
        }
    }
    // end::writeObjectsToFile[]

    // tag::readObjectsFromFile[]
    private static LocalDate readObjectsFromFile(Student[] students, String filename) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois =
                     new ObjectInputStream(
                             new FileInputStream(filename))
        ) {
            LocalDate date = (LocalDate) ois.readObject();
            students = (Student[]) ois.readObject();
            return date;
        }
    }
    // end::readObjectsFromFile[]
}
