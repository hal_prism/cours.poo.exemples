package fr.uvsq.info.poo.classes;

import fr.uvsq.info.poo.coo.Point2D;

// tag::cercle-static[]
/**
 * Un cercle en deux dimensions.
 *
 * @author Stephane Lopes
 * @version jan. 2017
 */
class Cercle2DWithCpt extends Cercle2D {
    /**
     * Le nombre d'instances de Cercle2DWithCpt
     */
    static int nbInstances = 0;

    /**
     * Initialise un cercle avec un centre et un rayon.
     *
     * @param centre Le centre.
     * @param rayon  Le rayon.
     */
    public Cercle2DWithCpt(Point2D centre, double rayon) {
        super(centre, rayon);
        ++nbInstances;
    }

    /**
     * Retourne le nombre d'instances de la classe.
     *
     * @return le nombre d'instances.
     */
    public static int getNbInstances() {
        return nbInstances;
    }

    /**
     * Décrémentation du nombre d'instances quand l'objet est détruit.
     */
    @Override
    protected void finalize() throws Throwable {
        --nbInstances;
        super.finalize();
    }
// end::cercle-static[]

    public static void main(String[] args) throws InterruptedException {
        // tag::cercle-static-main[]
        assert Cercle2DWithCpt.getNbInstances() == 0 :
                Cercle2DWithCpt.getNbInstances();

        // Creation des cercles
        Cercle2DWithCpt c1 = new Cercle2DWithCpt(new Point2D(2.0, 4.0),
                5.0);
        Cercle2DWithCpt c2 = new Cercle2DWithCpt(new Point2D(1.0, 2.0),
                4.0);
        Cercle2DWithCpt c3 = new Cercle2DWithCpt(new Point2D(3.0, 4.0),
                2.0);

        assert Cercle2DWithCpt.getNbInstances() == 3 :
                Cercle2DWithCpt.getNbInstances();

        // Simple liaison
        Cercle2DWithCpt unAutreC3 = c3;

        assert Cercle2DWithCpt.getNbInstances() == 3 :
                Cercle2DWithCpt.getNbInstances();

        // Suppression d'un instance
        c1 = null;
        System.gc();
        Thread.sleep(1000);

        assert Cercle2DWithCpt.getNbInstances() == 2 :
                Cercle2DWithCpt.getNbInstances();
        // end::cercle-static-main[]
    }
}
