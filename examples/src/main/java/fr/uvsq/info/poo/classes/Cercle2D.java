// tag::cercle-attr[]
package fr.uvsq.info.poo.classes;

import fr.uvsq.info.poo.coo.Point2D;

/**
 * Un cercle en deux dimensions.
 *
 * @version  jan. 2017
 * @author   Stéphane Lopes
 * 
 */
class Cercle2D {
    /** Le centre du cercle. */
    private Point2D centre;

    /** Le rayon du cercle */
    private final double rayon;
// end::cercle-attr[]
// tag::cercle-cons[]
    /**
     * Initialise un cercle avec un centre et un rayon.
     * @param centre Le centre.
     * @param rayon Le rayon.
     */
    public Cercle2D(final Point2D centre, final double rayon) {
        this.centre = centre;
        this.rayon = rayon;
    }

    /**
     * Initialise un cercle centré à l'origine et de rayon 1
     */
    public Cercle2D() {
        this(new Point2D(), 1.0);
    }
// end::cercle-cons[]

// tag::cercle-access[]
    /**
     * Renvoie le centre du cercle.
     * @return le centre du cercle.
     */
    public Point2D getCentre() {
        return centre;
    }

    /**
     * Renvoie le rayon du cercle.
     * @return le rayon du cercle.
     */
    public double getRayon() {
        return rayon;
    }
// end::cercle-access[]

// tag::cercle-mut[]
    /**
     * Translate le cercle.
     * @param dx deplacement en abscisse.
     * @param dy deplacement en ordonnees.
     */
    public void translate(final double dx, final double dy) {
        centre.translate(dx, dy);
    }
// end::cercle-mut[]

    /**
     * Retourne une chaine decrivant le cercle.
     * @return la representation textuelle du cercle.
     */
    @Override
    public String toString() {
        return String.format("[(%f, %f), %f]", centre.getAbscisse(), centre.getOrdonnee(), rayon);
    }
}
