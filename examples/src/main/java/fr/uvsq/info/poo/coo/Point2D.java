package fr.uvsq.info.poo.coo;

public class Point2D {
    /** L'abscisse du point. */
    private double x;

    /** L'ordonnée du point. */
    private double y;

    /**
     * Initialise un point à partir de deux coordonnées.
     *
     * @param x l'abscisse du point.
     * @param y l'ordonnée du point.
     */
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Initialise un point à partir d'une abscisse.
     * L'ordonnée est alors fixée à 0.
     *
     * @param x l'abscisse du point.
     */
    public Point2D(double x) {
        this(x, 0.0);
    }

    /**
     * Initialise un point à l'origine (0, 0).
     */
    public Point2D() {
        this(0.0, 0.0);
    }

    /**
     * Renvoie l'abscisse du point.
     *
     * @return l'abscisse du point.
     */
    public double getAbscisse() {
        return x;
    }

    /**
     * Renvoie l'ordonnée du point.
     *
     * @return l'ordonnée du point.
     */
    public double getOrdonnee() {
        return y;
    }

    /**
     * Renvoie le module du point.
     *
     * @return le module du point.
     */
    public double getModule() {
        return Math.hypot(x, y);
    }

    /**
     * Renvoie l'argument du point.
     *
     * @return l'argument du point.
     */
    public double getArgument() {
        return Math.atan2(x, y);
    }

    /**
     * Translate le point.
     *
     * @param dx déplacement en abscisse.
     * @param dy déplacement en ordonn�es.
     */
    public void translate(double dx, double dy) {
        x += dx;
        y += dy;
    }

    /**
     * Retourne une chaîne décrivant le point.
     *
     * @return la représentation textuelle du point.
     */
    public String toString() {
        return String.format("(%f, %f) [%f, %f]", x, y, getModule(), getArgument());
    }
}
