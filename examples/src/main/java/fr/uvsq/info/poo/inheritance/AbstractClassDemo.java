package fr.uvsq.info.poo.inheritance;

import javafx.geometry.Point2D;

/**
 * Représente l'application.
 *
 * @author Stéphane Lopes
 * @version jan. 2017
 */
enum AbstractClassDemo {
    ENVIRONNEMENT;

    /*
     * Méthode principale du programme.
     */
    public void run(String[] args) throws InterruptedException, CloneNotSupportedException {
        // tag::abstract-class-demo[]
        // Création du tableau de références
        final int NB_FIGURES = 4;
        FigureFermee2D[] figures = new FigureFermee2D[NB_FIGURES];

        // Création des formes
        figures[0] = new Rectangle2D(new Point2D(0.0, 5.0),
                new Point2D(2.0, 2.0));
        figures[1] = new Cercle2D(new Point2D(1.0, 2.0), 3.0);
        figures[2] = new Rectangle2D(new Point2D(5.0, 5.0),
                new Point2D(7.0, 3.0));
        figures[3] = new Cercle2D(new Point2D(4.0, 5.0), 2.0);

        // Réalise une translation de la figure
        for (int i = 0; i < figures.length; ++i) {
            figures[i].translate(1.0, 2.0);
        }
        // end::abstract-class-demo[]
    }

    public static void main(String[] args) throws InterruptedException, CloneNotSupportedException {
        ENVIRONNEMENT.run(args);
    }
}