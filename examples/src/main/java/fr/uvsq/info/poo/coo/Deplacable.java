package fr.uvsq.info.poo.coo;

public interface Deplacable {
    /**
     * Translate l'objet.
     *
     * @param dx deplacement en abscisse
     * @param dy deplacement en ordonnées
     */
    void translate(double dx, double dy);
}
