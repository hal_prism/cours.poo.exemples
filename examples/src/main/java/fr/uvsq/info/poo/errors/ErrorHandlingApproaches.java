package fr.uvsq.info.poo.errors;

import java.util.Optional;

import static java.lang.Math.sqrt;

public class ErrorHandlingApproaches {
    // tag::retcode[]
    public static double sqrtWithReturnCode(double d) {
        return Double.isNaN(d) || d < 0.0 ?
                Double.NaN :
                sqrt(d);
    }
    // end::retcode[]

    // tag::globalcode[]
    public static enum SqrtError { None, NegArg, NaNArg; }
    private static SqrtError sqrtError = SqrtError.None;
    public static SqrtError getSqrtError() { return sqrtError; }
    public static double sqrtWithGlobalCode(double d) {
        if (Double.isNaN(d)) {
            sqrtError = SqrtError.NaNArg;
        } else if (d < 0) {
            sqrtError = SqrtError.NegArg;
        }
        return sqrt(d);
    }
    // end::globalcode[]

    // tag::exception[]
    public static double sqrtWithException(double d) {
        if (Double.isNaN(d) || d < 0.0) {
            throw new IllegalArgumentException("Argument négatif ou NaN");
        }
        return sqrt(d);
    }
    // end::exception[]

    // tag::option[]
    public static Optional<Double> sqrtWithOption(double d) {
        return Double.isNaN(d) || d < 0.0 ?
                Optional.empty() :
                Optional.of(sqrt(d));
    }
    // end::option[]

    public static void main(String[] args) {
        double value = 4.0;

        {
            // tag::retcodeuse[]
            double result = sqrtWithReturnCode(value);
            if (Double.isNaN(result)) {
                System.err.println("Argument illégal (négatif ou égal à NaN).");
            } else {
                System.out.printf("sqrt(%f) = %f\n", value, result);
            }
            // end::retcodeuse[]
        }

        {
            // tag::globalcodeuse[]
            double result = sqrtWithGlobalCode(value);
            if (getSqrtError() != SqrtError.None) {
                System.err.println("Argument illégal (négatif ou égal à NaN).");
            } else {
                System.out.printf("sqrt(%f) = %f\n", value, result);
            }
            // end::globalcodeuse[]
        }

        {
            // tag::exceptionuse[]
            double result;
            try {
                result = sqrtWithException(value);
                System.out.printf("sqrt(%f) = %f\n", value, result);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace(System.err);
            }
            // end::exceptionuse[]
        }

        {
            // tag::optionuse[]
            Optional<Double> result = sqrtWithOption(value);
            System.out.printf("sqrt(%f) = %f\n", value, result.orElse(Double.NaN));
            // end::optionuse[]
        }
    }
}
