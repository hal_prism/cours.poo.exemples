package fr.uvsq.info.poo.inheritance;

import javafx.geometry.Point2D;

// tag::rect[]
/**
 * Un rectangle en deux dimensions.
 * Les côtés du rectangle sont toujours parallèles aux axes.
 *
 * @version  jan. 2017
 * @author   Stéphane Lopes
 * 
 */
class Rectangle2D extends FigureFermee2D implements Cloneable {
    /** Coordonnées du coin supérieur gauche */
    private Point2D orig;

    /** Coordonnées du coin inférieur droit */
    private Point2D fin;

    /**
     * Initialise le rectangle.
     * @param supGauche Le coin supérieur gauche.
     * @param infDroit Le coin inférieur droit.
     */
    public Rectangle2D(Point2D supGauche, Point2D infDroit) {
        assert supGauche.getX() <= infDroit.getX() &&
               supGauche.getY() >= infDroit.getY();
        orig = supGauche;
        fin = infDroit;
    }

    public Point2D getSupGauche() { return orig; }

    public double getLargeur() {
        return fin.getX() - orig.getX();
    }

    public double getHauteur() {
        return orig.getY() - fin.getY();
    }

    /**
     * Retourne une description du rectangle.
     * @return la description.
     */
    public String getDesc() {
        return String.format("O = %s L = %s, H = %s", orig, getLargeur(), getHauteur());
    }
// end::rect[]

    // tag::rect-tostring[]
    /**
     * Retourne une chaîne représentant l'objet.
     * @return la chaîne.
     */
    @Override
    public String toString() {
        return String.format("O = %s L = %s, H = %s", orig, getLargeur(), getHauteur());
    }
    // end::rect-tostring[]

    /**
     * Retourne une copie "profonde" de l'objet.
     * @return la copie.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        Rectangle2D r = (Rectangle2D)super.clone();
        r.orig = new Point2D(orig.getX(), orig.getY());
        r.fin = new Point2D(fin.getX(), fin.getY());
        return r;
    }

    // tag::rect-equals[]
    /**
     * Teste l'égalité de deux rectangles.
     * @param obj le rectangle à comparer.
     * @return true si les objets sont égaux.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Rectangle2D) {
            Rectangle2D r = (Rectangle2D)obj;
            return orig.equals(r.orig) && fin.equals(r.fin);
        }
        return false;
    }

    /**
     * Retourne une valeur de hashage pour l'objet.
     * @return la valeur de hashage.
     */
    @Override
    public int hashCode() {
        return orig.hashCode() ^ fin.hashCode();
    }
    // end::rect-equals[]

    // tag::rect-translate[]
    /**
     * Translate le rectangle.
     * @param dx déplacement en abscisse.
     * @param dy déplacement en ordonnées.
     */
    @Override
    public void translate(double dx, double dy) {
        orig.add(dx, dy);
        fin.add(dx, dy);
    }
    // end::rect-translate[]
}
