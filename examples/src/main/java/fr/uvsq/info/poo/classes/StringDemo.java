package fr.uvsq.info.poo.classes;

//TODO move this into a test class
public class StringDemo {
    public static void main(String[] args) {
        // tag::string-demo[]
        String source = "abcde";
        int len = source.length();
        StringBuilder dest = new StringBuilder(len);

        for (int i = (len - 1); i >= 0; --i) {
            dest.append(source.charAt(i));
        }

        assert dest.toString().equals("edcba") : dest;
        // end::string-demo[]
    }
}
