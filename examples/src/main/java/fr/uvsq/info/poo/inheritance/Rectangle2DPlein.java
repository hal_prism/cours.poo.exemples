package fr.uvsq.info.poo.inheritance;


import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

// tag::rect-plein-1[]
/**
 * Un rectangle plein en deux dimensions.
 *
 * @author Stéphane Lopes
 * @version nov. 2008
 */
class Rectangle2DPlein extends Rectangle2D {
    /**
     * La couleur de remplissage
     */
    private Color couleur;

    /**
     * Initialise le rectangle plein.
     *
     * @param supGauche Le coin supérieur gauche.
     * @param infDroit  Le coin inférieur droit.
     * @param couleur   La couleur de remplissage.
     */
    public Rectangle2DPlein(Point2D supGauche,
                            Point2D infDroit,
                            Color couleur) {
        super(supGauche, infDroit);
        assert couleur != null;
        this.couleur = couleur;
    }

    /**
     * Renvoie la couleur.
     *
     * @return la couleur.
     */
    public Color getCouleur() {
        return couleur;
    }
// end::rect-plein-1[]

    /**
     * Retourne une description du rectangle plein.
     *
     * @return la description.
     */
    @Override
    public String getDesc() {
        return String.format("%s, couleur : %s", super.getDesc(), couleur);
    }

    // tag::rect-plein-tostring[]
    /**
     * Retourn une chaîne représentant l'objet.
     *
     * @return la chaîne.
     */
    @Override
    public String toString() {
        return String.format("%s, couleur : %s", super.getDesc(), couleur);
    }
    // end::rect-plein-tostring[]

    public static void main(String[] args) throws CloneNotSupportedException {
        // tag::rect-plein-use-1[]
        // Déclaration et création d'un rectangle rouge
        Rectangle2DPlein rp = new Rectangle2DPlein(new Point2D(1.0, 2.0),
                new Point2D(3.0, 0.0),
                Color.RED);
        assert rp.getCouleur() == Color.RED;

        // Déclaration d'un rectangle et
        // liaison avec un rectangle plein
        Rectangle2D r = new Rectangle2DPlein(new Point2D(1.0, 2.0),
                new Point2D(2.0, 1.0),
                Color.YELLOW);
        assert r.getLargeur() == 1;
        // end::rect-plein-use-1[]

        {
            // tag::rect-plein-use-2[]
            // Création d'un tableau de références sur des Rectangle2D
            final int NB_RECTANGLES = 2;
            Rectangle2D[] figures = new Rectangle2D[NB_RECTANGLES];

            // Un rectangle
            figures[0] = new Rectangle2D(new Point2D(0.0, 5.0),
                    new Point2D(2.0, 2.0));

            // Un rectangle plein
            figures[1] = new Rectangle2DPlein(new Point2D(1.0, 3.0),
                    new Point2D(3.0, 2.0),
                    Color.BLUE);

            // getDesc() de Rectangle2D
            assert figures[0].getDesc().equals("O = (0.0, 5.0) L = 2.0 H = 3.0");

            // getDesc() de Rectangle2DPlein
            assert figures[1].getDesc().equals("O = (1.0, 3.0) L = 2.0 H = 1.0, couleur : (0, 0, 255)");
            // end::rect-plein-use-2[]

            // tag::rect-plein-use-3[]
            // Test de toString
            assert figures[0].toString().equals("O = (0.0, 5.0) [5.0, 0.0] L = 2.0 H = 3.0");
            assert figures[1].toString().equals("O = (1.0, 3.0) [3.1622776601683795, 0.3217505543966422] L = 2.0 H = 1.0 couleur = java.awt.Color[r=0,g=0,b=255]");
            // end::rect-plein-use-3[]

            // Test du clonage d'objets
            Rectangle2D copie = (Rectangle2D) figures[0].clone();
            assert copie != figures[0]; // Pas identiques
            assert copie.getClass() == figures[0].getClass(); // Même classe
            assert copie.equals(figures[0]);                  // Egaux

            // Test de l'égalité
            // tag::rect-plein-use-4[]
            Rectangle2D r1 = new Rectangle2D(new Point2D(0.0, 5.0),
                    new Point2D(2.0, 2.0));
            Rectangle2D r2 = new Rectangle2D(new Point2D(0.0, 5.0),
                    new Point2D(2.0, 2.0));
            Rectangle2D r3 = new Rectangle2D(new Point2D(0.0, 5.0),
                    new Point2D(2.0, 2.0));
            assert r1.equals(r1);    // Réflexivité
            assert r1.equals(r2) && r2.equals(r1); // Symétrie
            assert r1.equals(r2) && r2.equals(r3) &&
                    !r1.equals(r3) == false; // Transitivité
            assert r1.equals(null) == false;
            assert r1.hashCode() == r2.hashCode();
            // end::rect-plein-use-4[]
        }
    }
}
