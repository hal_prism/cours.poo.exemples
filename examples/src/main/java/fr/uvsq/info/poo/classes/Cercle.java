package fr.uvsq.info.poo.classes;

import fr.uvsq.info.poo.coo.Point2D;
import javafx.geometry.Point3D;

// tag::cercle-generic[]
/**
 * Un cercle générique.
 * Ce cercle peut s'adapter au cas à 2 ou à 3 dimensions.
 *
 * @version  jan. 2017
 * @author   Stephane Lopes
 * 
 */
class Cercle<T> {
    /** Le centre du cercle. */
    private T centre;

    /** Le rayon du cercle */
    private double rayon;

    /**
     * Initialise un cercle avec un centre et un rayon.
     * @param centre Le centre.
     * @param rayon Le rayon.
     */
    public Cercle(T centre, double rayon) {
        this.centre = centre;
        this.rayon = rayon;
    }

    public T getCentre() {
        return centre;
    }

    public double getRayon() {
        return rayon;
    }
// end::cercle-generic[]

    public static void main(String[] args) {
        // tag::cercle-generic-main[]
        // En 2 dimensions
        Point2D p1 = new Point2D(1.0, 2.0);
        Cercle<Point2D> c1 = new Cercle<Point2D>(p1, 3.0);

        // Invocation d'une methode de Point2D
        double xCentre = c1.getCentre().getAbscisse();

        // En 3 dimensions
        Point3D p2 = new Point3D(3.0, 4.0, 5.0);
        Cercle<Point3D> c2 = new Cercle<Point3D>(p2, 3.0);

        // Invocation d'une methode de Point3D
        double zCentre = c2.getCentre().getZ();
        // end::cercle-generic-main[]
    }
}
