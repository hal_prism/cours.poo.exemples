package fr.uvsq.info.poo.collections;

import java.util.*;

public class ExercicesCollectionsSolutions {
    private static final int MAX = 10;

    public static void main(String[] args) {
        exerciceListe();
        exerciceMap();
    }

    private static void exerciceListe() {
        // tag::exerciceliste1[]
        List<Integer> uneListe = new ArrayList<>();
        for (int i = 0; i < MAX; ++i) uneListe.add(i);
        // end::exerciceliste1[]
        System.out.println(uneListe);

        // tag::exerciceliste2[]
        Collections.shuffle(uneListe);
        // end::exerciceliste2[]

        // tag::exerciceliste3[]
        System.out.println(uneListe);
        for (ListIterator<Integer> it = uneListe.listIterator(uneListe.size());
             it.hasPrevious(); ) {
            System.out.print(it.previous());
            System.out.print(", ");
        }
        // end::exerciceliste3[]
        System.out.println();

        // tag::exerciceliste4[]
        Collections.sort(uneListe);
        // end::exerciceliste4[]
        System.out.println(uneListe);

        // tag::exerciceliste5_1[]
        uneListe.sort((i1, i2) -> i2 - i1);
        // end::exerciceliste5_1[]
        System.out.println(uneListe);

        Collections.shuffle(uneListe);
        System.out.println(uneListe);

        // tag::exerciceliste5_2[]
        uneListe.sort(Collections.reverseOrder());
        // end::exerciceliste5_2[]
        System.out.println(uneListe);

        /*
        // tag::exerciceliste6[]
        // Remplacer
        List<Integer> uneListe = new ArrayList<>();
        // Par
        List<Integer> uneListe = new LinkedList<>();
        //
        // Le reste du programme ne change pas !
        //
        // end::exerciceliste6[]
        */
    }

    private static void exerciceMap() {
        // tag::exercicemap1[]
        Map<Integer, Integer> uneMap = new HashMap<>();
        for (int i = 0; i < MAX; ++i) uneMap.put(i, i * i * i);
        // end::exercicemap1[]
        System.out.println(uneMap);

        // tag::exercicemap2[]
        List<Integer> lesCubes = new ArrayList<>(uneMap.values());
        Collections.sort(lesCubes);
        // end::exercicemap2[]
        System.out.println(lesCubes);

        // tag::exercicemap3[]
        for (Map.Entry<Integer, Integer> elt : uneMap.entrySet()) {
            if (elt.getKey() % 2 == 0) {
                System.out.println(elt.getKey() + " -> " + elt.getValue());
            }
        }
        // end::exercicemap3[]
    }
}
