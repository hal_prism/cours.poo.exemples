package fr.uvsq.info.poo.classes;

/**
 * Représente l'application.
 *
 * @version  jan. 2017
 * @author   Stéphane Lopes
 * 
 */
enum ApplicationVide {
    ENVIRONNEMENT;
    
    /*
     * Méthode principale du programme.
     * @param args les arguments de ligne de commande
     */
    public void run(String[] args) {
      // ...
    }
   
    /*
     * Point d'entrée du programme.
     * @param args les arguments de ligne de commande
     */
    public static void main(String[] args) {
      ENVIRONNEMENT.run(args);
    }
}

