package fr.uvsq.info.poo.exercise

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test

class ExercisePlugin implements Plugin<Project> {
    Project project

    void apply(Project project) {
        this.project = project
        def extension = project.extensions.create('exercise', ExercisePluginConfiguration, project)

        project.task('showConfiguration') {
            doLast {
                def exerciseString = loadExerciseProperties().exercise
                println """
Current exercice number: $exerciseString 
Exercise plugin configuration:
    - configuratio file = ${project.exercise.configFile}
    - exercise directory = ${project.exercise.exerciseDirectory}
"""
            }
        }

        project.task('resetExercises') {
            doLast {
                modifyExerciseProperty('none')
            }
        }

        project.task('allExercises') {
            doLast {
                modifyExerciseProperty('all')
            }
        }

        project.task('nextExercises') {
            doLast {
                def exerciseString = loadExerciseProperties().exercise
                if (exerciseString == 'all') return
                def exercises = availableExercises()
                if (exercises.isEmpty()) return
                if (exerciseString == 'none') {
                    exerciseString = exercises.get(0).join('.')
                } else {
                    def currentExercise = exerciseString.split(/\./)
                    def exerciseInList = exercises.find { it == currentExercise }
                    assert exerciseInList != null
                    def exerciseIndex = exercises.indexOf(exerciseInList)
                    exerciseString = exerciseIndex == exercises.size() - 1 ? 'all' : exercises.get(exerciseIndex + 1).join('.')
                }
                modifyExerciseProperty(exerciseString)
            }
        }

        project.tasks.withType(Test) {
            def props = loadExerciseProperties()
            systemProperties props.subMap(['exercise'])
        }
    }

    Properties loadExerciseProperties() {
        File exerciseProperties = project.file(project.exercise.configFile)
        def props = new Properties()
        exerciseProperties.withReader {
            props.load it
        }
        props
    }

    void modifyExerciseProperty(String newValue) {
        File exerciseProperties = project.file(project.exercise.configFile)
        def props = new Properties()
        props.setProperty('exercise', newValue)
        props.store new FileWriter(project.file(exerciseProperties)), 'Exercise modified'
    }

    List availableExercises() {
        def exerciseDirectories = []
        def exercisePath = project.file(project.exercise.exerciseDirectory)
        exercisePath.eachDirMatch(~/exercise\d_\d$/) {
            exerciseDirectories << it
        }
        exerciseDirectories.collect {
            it.getName()[-3..-1].split('_')
        }.sort { a,b -> a[0] <=> b[0] ?: a[1] <=> b[1] }
    }
}

class ExercisePluginConfiguration {
    private static final String PROPERTY_FILE = 'config/exercise/exercises.properties'
    private static final String EXERCISE_DIRECTORY = 'exercises/src/main/java/fr/uvsq/info/poo'

    Project project
    String configFile
    String exerciseDirectory

    ExercisePluginConfiguration(Project project) {
        this.project = project
        configFile = project.file("$project.rootDir/$PROPERTY_FILE")
        exerciseDirectory = project.file("$project.rootDir/$EXERCISE_DIRECTORY")
    }
}
