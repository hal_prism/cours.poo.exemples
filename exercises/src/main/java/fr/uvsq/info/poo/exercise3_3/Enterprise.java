package fr.uvsq.info.poo.exercise3_3;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe <code>Enterprise</code> représente une entreprise.
 * La seule fonctionnalité de cette classe est d'embaucher des employés.
 *
 * @author hal
 * @version 2017-2018
 */
public class Enterprise {
  /**
   * Nom de l'entreprise.
   */
  private final String name;

  /**
   * Les employés de l'entreprise.
   */
  private List<Employee> employees;

  /**
   * Initialise l'entreprise avec son nom.
   *
   * @param name le nom de l'entreprise
   */
  public Enterprise(final String name) {
    this.name = name;
    employees = new ArrayList<>();
  }

  /**
   * L'entreprise embauche un nouvel employé.
   *
   * @param anEmploye l'employé à embaucher
   */
  public void embauche(Employee anEmploye) {
    employees.add(anEmploye);
  }
}
