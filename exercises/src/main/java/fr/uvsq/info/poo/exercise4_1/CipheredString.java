package fr.uvsq.info.poo.exercise4_1;

class CipheredString {

  private static final char MIN_CHAR = 'A';
  private static final char MAX_CHAR = 'Z';
  private static final int NUMBER_OF_LETTERS = 26;

  public CipheredString(String aString, int shift) {

  }

  /**
   * Décale un caractère représentant une lettre majuscule.
   * Les lettres sont décalées de <code>shift</code> caractères de façon circulaire.
   * Les autres caractères ne sont pas modifiés.
   *
   * @param c     le caractère à décaler
   * @param shift le décalage à appliquer
   * @return le caractère décalé
   */
  static char shiftCharacter(char c, int shift) {
    char resultedChar = c;
    if (c >= MIN_CHAR && c <= MAX_CHAR) {
      int charCode = c - MIN_CHAR;
      int shiftedCharCode = (charCode + shift) % NUMBER_OF_LETTERS;
      resultedChar = (char) (shiftedCharCode < 0 ? MAX_CHAR + shiftedCharCode + 1 : shiftedCharCode + MIN_CHAR);
    }
    return resultedChar;
  }
}
