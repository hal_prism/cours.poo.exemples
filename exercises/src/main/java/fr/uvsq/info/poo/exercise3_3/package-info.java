/**
 * Dans cette exercice, vous commencerez à modifier une classe Java.
 * Cet exercice utilise le <a href="http://e-campus2.uvsq.fr/Members/steplope/Fichiers/entreprise-bluej.zip/at_download/file">projet Entreprise</a>
 * à ouvrir sous BlueJ (<em>Project/Open Non Bluej</em>).
 * <ol>
 * <li>Modifiez la classe Employe comme suit :
 *     <ol>
 *         <li>ajoutez l’attribut privé age,
 *         <li>modifiez les méthodes pour prendre en compte ce changement,
 *         <li>recompilez et testez ce changement.
 *     </ol>
 * <li>Mettez à jour les commentaires Javadoc et vérifier la documentation de la classe.
 * <li>Ajoutez la méthode toString à la classe Entreprise : cette méthode retourne une chaîne de caractères
 * contenant le nom de l’entreprise et la liste de ses employés (prénom, nom et âge),
 * <li>recompiler les deux classes et tester les nouvelles méthodes.
 * </ol>
 */
package fr.uvsq.info.poo.exercise3_3;
