package fr.uvsq.info.poo.exercise3_3;

/**
 * La classe <code>Employee</code> représente les employés d'une entreprise.
 *
 * @author Stéphane Lopes
 * @version 2017-2018
 */
public class Employee {
  /**
   * Le prénom de l'employé.
   */
  private final String firstname;
  /**
   * Le nom de l'employé.
   */
  private final String lastname;

  /**
   * Initialise un employé avec un prénom et un nom.
   *
   * @param firstname le prénom de l'employé
   * @param lastname  le nom de l'employé
   * @param age       l'age de l'employé
   */
  public Employee(String firstname, String lastname, int age) {
    this.firstname = firstname;
    this.lastname = lastname;
  }

  /**
   * Retourne une chaîne de caractères représentant l'employé.
   * Le format utilisé est "nom prénom".
   *
   * @return une chaîne contenant le prénom suivi du nom de l'employé
   */
  @Override
  public String toString() {
    return firstname + ' ' + lastname;
  }
}
