package fr.uvsq.info.poo.exercise4_2

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '4.2'] })
class ServerShould extends Specification {
    def "broadcast messages to clients"() {
        given:
        def server = new Server();
        def c1 = Mock(Client)
        server.register(c1)
        def c2 = Mock(Client)
        server.register(c2)
        def c3 = Mock(Client)
        c3.getName() >> "c3"
        server.register(c3)

        when:
        server.broadcast(c3, "Bonjour")

        then:
        1 * c1.receive("[c3] Bonjour")
        1 * c2.receive("[c3] Bonjour")
        1 * c3.receive("[c3] Bonjour")
    }
}
