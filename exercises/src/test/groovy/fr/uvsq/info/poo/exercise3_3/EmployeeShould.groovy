package fr.uvsq.info.poo.exercise3_3

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '3.3'] })
class EmployeeShould extends Specification {
    def "have an attribute age (question 1)"() {
        expect:
        employee.toString() == description

        where:
        employee | description
        new Employee("Ariane", "Dupond", 29) | "Ariane Dupond (29)"
        new Employee("Cassiopée", "Durand", 28) | "Cassiopée Durand (28)"
        new Employee("Hélios", "Skywalker", 24) | "Hélios Skywalker (24)"
    }
}
