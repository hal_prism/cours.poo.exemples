package fr.uvsq.info.poo.exercise4_2

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '4.2'] })
class ClientShould extends Specification {
    def "be able to connect to a server"() {
        given:
        def server = Mock(Server)
        def client = new Client('irc_client')

        when:
        client.connectTo(server)

        then:
        1 * server.register(client)
    }

    def "send message when connected"() {
        given:
        def server = Mock(Server)
        def client = new Client('irc_client')
        client.connectTo(server)

        when:
        client.send("some message")

        then:
        1 * server.broadcast(client, "some message")
    }
}
