package fr.uvsq.info.poo.exercise4_2

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '4.2'] })
class IntegrationScenario extends Specification {
    def "Received sessage should be broadcasted to all connected clients"() {
        given:
        def server = new Server();
        def c1 = Spy(Client, constructorArgs: ["c1"])
        c1.connectTo(server)
        def c2 = Spy(Client, constructorArgs: ["c2"])
        c2.connectTo(server)
        def c3 = Spy(Client, constructorArgs: ["c3"])
        c3.connectTo(server)

        when:
        c3.send("Bonjour")

        then:
        1 * c1.receive("[c3] Bonjour")
        1 * c2.receive("[c3] Bonjour")
        1 * c3.receive("[c3] Bonjour")
    }
}
