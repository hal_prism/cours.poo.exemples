package fr.uvsq.info.poo.exercise4_1

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '4.1'] })
class CipheredStringShould extends Specification {
    def "be initialized with an unciphered string and a shift"() {
        when:
        def hal = new CipheredString('HAL', 1)

        then:
        hal.toString() == 'HAL'
    }

    def "be initialized with a null string and a shift"() {
        when:
        def nullString = new CipheredString(null, 1)

        then:
        nullString.toString() == ''
    }

    def "be initialized with an unciphered string and a shift (Factory method)"() {
        when:
        def hal = CipheredString.fromUncipheredString('HAL', 1)

        then:
        hal.toString() == 'HAL'
    }

    def "be initialized with a ciphered string and a shift (Factory method)"() {
        when:
        def hal = CipheredString.fromCipheredString('IBM', 1)

        then:
        hal.toString() == 'HAL'
    }

    def "uncipher should return the unciphered string"() {
        given:
        def hal = new CipheredString('HAL', 1)

        when:
        def result = hal.uncipher()

        then:
        result == 'HAL'
    }

    def "cipher should return the ciphered string"() {
        given:
        def hal = new CipheredString('HAL', 1)

        when:
        def result = hal.cipher()

        then:
        result == 'IBM'
    }

    def "have a correct shiftCharacter method"() {
        expect:
        CipheredString.shiftCharacter(character as char, shift) == shiftedCharacter

        where:
        character | shift | shiftedCharacter
        'h' as char |  5 | 'h' as char
        'h' as char | -5 | 'h' as char
        ' ' as char |  3 | ' ' as char
        ' ' as char | -3 | ' ' as char
        'H' as char |  1 | 'I' as char
        'I' as char | -1 | 'H' as char
        'A' as char |  1 | 'B' as char
        'B' as char | -1 | 'A' as char
        'L' as char |  1 | 'M' as char
        'M' as char | -1 | 'L' as char
        'Y' as char |  3 | 'B' as char
        'B' as char | -3 | 'Y' as char
        'Y' as char | 29 | 'B' as char
        'B' as char |-29 | 'Y' as char
    }
}
