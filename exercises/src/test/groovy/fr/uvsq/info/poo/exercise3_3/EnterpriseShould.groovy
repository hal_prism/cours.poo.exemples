package fr.uvsq.info.poo.exercise3_3

import spock.lang.Requires
import spock.lang.Specification

@Requires({ !sys.containsKey('exercise') || sys.exercise in ['all', '3.3'] })
class EnterpriseShould extends Specification {
    def "have a toString method which display name and employees"() {
        given:
        def enterprise = new Enterprise("ACME")
        def ariane = new Employee("Ariane", "Dupond", 29)
        def cassiopee = new Employee("Cassiopée", "Durand", 28)
        def helios = new Employee("Hélios", "Skywalker", 24)

        when:
        enterprise.embauche(ariane)
        enterprise.embauche(cassiopee)
        enterprise.embauche(helios)

        then:
        enterprise.toString() == "ACME [Ariane Dupond (29), Cassiopée Durand (28), Hélios Skywalker (24)]"
    }
}
